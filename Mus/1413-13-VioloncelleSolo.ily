﻿\version "2.16.0"      % Fauré - Elégie - Violoncelle Solo


\relative c{
\clef bass

\override DynamicLineSpanner #'staff-padding = #2.4



R1
ees'4\f d c8( d) c( g)
bes4 aes g8( aes) g( ees)
g4_\markup {\italic "sempre " \dynamic f} f ees8(f) ees( c)
ees4 d c2 \once \override TextScript #'padding = #1.5
ees'4(\pp-\tag #'partie ^\markup{\small 2 \raise #1.0 {\tiny a} \small Corda} d) c8( d c g)
bes4( aes) g8( aes g ees)
g4( f) ees8(f ees c)
ees4( d) c2
g'4\p g4.->\< g8( aes\! c\>)
<<b4 {s8 s8\!}>> \crescTextCresc b4.->\< c8( \clef tenor ees aes)\! \crescHairpin
g4.\< g8 bes\f( aes) g( f)
ees4.( c8\>) bes2
bes4\p bes4.->\< aes8( g f)\! \crescTextCresc \set crescendoText = \markup {\italic \fontsize #-1 {"molto cresc."}}
f'4\< f4. ees8( d c)\! \crescHairpin
fis4\ff\> g8 g,\! fis'4\> g8 g,\!
fis'4 g2.\>
ees4\ppp( d) c8( d c g) \clef bass
bes4( aes) g8(aes g ees)
g4( f) ees8( f ees c)
ees4( d) c2
ees4( d) c2
r2^\markup{\italic \small {"sempre molto adagio"}} aes8\pp( bes aes ees)
aes( bes aes ees) bes'( c bes a)
g( a bes aes) g( ees) aes4
des8( ees des ees,) c'( des c ees,)
des'( ees des ees,) c'( des c ees,)
f'(\semprepp g f bes,) g'( aes g bes,)
a'( aes g bes,) a'( aes g f) \clef tenor %%%%%%%%%%%%\once
r16_\markup{\italic \small "espressivo"} ees'8 des ees16 \times 2/3{c16[( des ees)]} ees16( g,8) aes( g16) \times 2/3{f[( c' f)]} \override TupletNumber #'transparent = ##t
f16\<( bes,8 c16) \times 2/3 {des16[( c des)]} \times 2/3 {d\!\>[( cis d)]} ees8.\!( bes16) des8 c
r16 a8( c16) \times 2/3 {a16[( bes c]} \times 2/3 {des16[ ees f])} \once \override TextScript #'padding = #2.0 f16(_\markup{\italic \small "poco  a  poco  cresc."} d8 f16) \times 2/3{d16[( ees f]} \times 2/3{g16[ aes c,)]}
c16( a8 bes16) \times 2/3{d16[( ees g]} \times 2/3{aes!16[ c c,)]} c16\<( bes8 b!16) \times 2/3{c16[( b c)]} \times 2/3{e16[( f g\!)]}
aes16\f( bes) aes( aes,) \clef bass \times 2/3{c16[( d c)]} \times 2/3{aes16[( f c)]} ees16( f) ees( fis,) \times 2/3{c'16[( d c)]} \times 2/3{ees,16[( d c)]} \override TupletNumber #'transparent = ##f
g'4 r \times 4/6{g''32\ff[( aes g) d( ees d)]} \times 4/6{g,[( aes g) d( ees d)]} \times 4/6{g,[( aes g) d'( ees d)]} \times 4/6{g[( aes g) d'( ees d)]} \override TupletNumber #'transparent = ##t
g8 r r4 \times 4/6{g32[( aes g) des( ees des)]} \times 4/6{g,[( aes g) des( ees des)]} \times 4/6{g,[( aes g) des'( ees des)]} \times 4/6{g[( aes g) des'( ees des)]}
g8 r \times 4/6{aes,,32[->( bes aes bes c d!)]} \times 4/6{e[( f g aes bes c)]} r4 \times 4/6{c,32[->( b c d ees d)]} \times 4/6{f[( ees d c b c)]}
\times 4/6{g[( aes g aes bes c]} \times 4/6{d[ ees f g aes g)]} \times 4/6{f[( ees d ees d c]} \times 4/6{bes[ aes g aes bes c])} \times 4/6{d[( ees f g a b)]} \clef tenor \times 4/6{c[( d ees f g aes)]} \times 4/6{g[( f ees) d( ees f)]} \times 4/6{fis--[ g-- a-- b-- c-- d--]}
ees4\ff d c8 d c g
bes4 aes g8 aes g ees
g4 f ees8 f ees c
ees4( des) c2\> \clef bass
ees,4( des) c2\!
ees,4\p( des)
c1\pp(
c1) \clef tenor
r16 c''8\pp des c16 \times 2/3{bes[(\< c16 des)]} des16( ees8) f16\!~ \times 2/3{f16[\> g( aes)]} \times 2/3{ees[( c aes)]\!} \clef bass
g16( aes8 g) f16( \times 2/3{g16[ aes bes)]} b16( c8 d16)~ \times 2/3{d16[ ees( c]} \times 2/3{g16[ ees f)]}
g16(_\markup{\italic \small "sempre dim."} aes8 g) f16( \times 2/3{g16[ aes bes)]} b16( c8 d16)~ \times 2/3{d16[ ees( c]} \times 2/3{g16[ aes bes)]}
b16\ppp( c8 d16)( \times 2/3{d16[) ees( c]} \times 2/3{g16[ aes bes)]} b16( c8 g) ees( d16)
b8( c) r4 b16( c8 g) ees( d16)
c1~
c2 r


}
