﻿\version "2.16.0"      % Fauré - Elégie - Cor 4

\relative c''{
\clef treble

\override DynamicLineSpanner #'staff-padding = #2.4




R1*9





 


R1*7






r4 r8 \addStacc { d,\mf(\> d d d d\!
d\pp) } r r4 r2
R1*4



R1*3


R1*4



R1*3


r2 r4 r8 g\<
ees4\!\f ees16-> r r8 e4 e16-> r r8
d8->\ff ees-> d-> a-> \once \override TextScript #'outside-staff-priority = #1 d16->\sec r r8 r4
f4.-> ees8-> f16->\sec r r8 r4
g,4->\sempreff g16-> r r8 g4->g16-> r r8
d'-> r r4 d8-> r r4
R1*5




R2\un
R1*9








}

