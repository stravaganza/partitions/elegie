﻿\version "2.16.0"      % Fauré - Elégie - Hautbois 2

\relative c''{
\clef treble

\override DynamicLineSpanner #'staff-padding = #2.4




ees,2\f\> ~ ees8\! r r4
R1*8




 


R1*6





fis4^>\f(\> g8)\! r fis4^>\f(\> g8)\! r
fis4^>\f(\> g8)\! r r2
R1*5




R1*3


R1*4



R1*4



r4 c16->\f r r8 r4 c16-> r r8
d2->\ff \once \override TextScript #'outside-staff-priority = #1 d16->\sec r r8 r4
des4.-> f,8-> des'16->\sec r r8 r4
g,8->\sempreff e-> f16-> r r8 f-> d-> ees!16-> r r8
c'-> r r4 b8-> r r4
R1*5




R2\un
R1*9








}

