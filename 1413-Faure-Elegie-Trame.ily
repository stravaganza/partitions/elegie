﻿\version "2.16.0"      % Fauré: Elégie - mesures et tempi avec tonalité

{
\tempo "Molto adagio" 8 = 69
\time 4/4
\tag #'Ton {\key c \minor}
\tag #'TonClarinette {\key d \minor}
\tag #'SansTon {\key c \major}

s1*9
\mark #1 s1*8
\mark #2 s1*5
\mark #3 s1*3
\mark #4 s1*3
s1^\markup {"poco rit."}
\mark #5 s1	^\markup {"a tempo"}
s1*3
\mark #6 s1
s1^\markup {"poco piu animato"}
s1*2
s2. s4^\markup {"rit."}
\mark #7 s1^\markup {"a tempo"}
s1*4
\time 2/4 s2
\mark #8 \time 4/4
s1*9
\bar "|."
}