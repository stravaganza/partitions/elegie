﻿\version "2.16.0"

cCompositeur = \markup { "Gabriel FAURÉ"}
cDatesCompositeur = "(1845-1924)"
cTitre = \markup {"ÉLÉGIE"}
cSousTitre = "op.24"
cRefConcert = "1413"
cCopyright = \markup {\fontsize #-4 \concat {
				\epsfile #X #2  #"cc.eps"  " 2012-" \italic {"Le Concert - "} \cRefConcert " / " \cCompositeur " - " \cTitre }}



%%%%	fonctions diverses -------------------------------------------------------------------------------
arco = ^\markup {"Arco"}
pizz = ^\markup {"Pizz."}
sourdOn = _\markup {\italic \fontsize #-2 \column {"mettez la" "sourdine"}}
divis = ^\markup {\fontsize #-1 "Div."}
simile = _\markup {\italic \fontsize #-1 "simile"}
dolce = _\markup {\italic \fontsize #-1 "dolce"}
dolcissimo = _\markup {\italic \fontsize #-1 "dolcissimo"}
dolcespr = _\markup {\italic \fontsize #-1 "dolce espressivo"}
ten = ^\markup {\italic "ten."}
sec = ^\markup {\italic \fontsize #-1 "sec"}
%rit = ^\markup {"rit."}
%pocorit = ^\markup {"poco rit."}
%atempo = ^\markup {"a tempo"}
anim = ^\markup {"Animato"}
sempreff = _\markup {\dynamic ff \italic "sempre "}
semprep = _\markup {\italic "sempre " \dynamic p}
semprepp = _\markup {\italic "sempre " \dynamic pp}
%pocoanim = ^\markup {"poco più animato"}
un = -\tag #'partie ^\markup{\huge \bold "1"}
reperes = {	\set Score.markFormatter = #format-mark-box-alphabet
						\override Score.RehearsalMark #'padding = #2.0 }
silencesMulti = {	\set Score.skipBars = ##t 
									\override MultiMeasureRest #'expand-limit = #1 }

	%%% staccato ..........................................
	#(define (make-script x)
		(make-music 'ArticulationEvent
               'articulation-type x))

	#(define (add-script m x)
		(case (ly:music-property m 'name)
     ((NoteEvent) (set! (ly:music-property m 'articulations)
                      (append (ly:music-property m 'articulations)
                         (list (make-script x))))
                   m)
     ((EventChord)(set! (ly:music-property m 'elements)
                      (append (ly:music-property m 'elements)
                         (list (make-script x))))
                   m)
     (else #f)))

	#(define (add-staccato m)
         (add-script m "staccato"))

	addStacc = #(define-music-function (parser location music)
                 (ly:music?)
           (map-some-music add-staccato music)) 
	%%% fin staccato ............................................

	%%% tenuto ..........................................
	#(define (make-script x)
		 (make-music 'ArticulationEvent
								 'articulation-type x))

	#(define (add-script m x)
   (case (ly:music-property m 'name)
     ((NoteEvent) (set! (ly:music-property m 'articulations)
                      (append (ly:music-property m 'articulations)
                         (list (make-script x))))
                   m)
     ((EventChord)(set! (ly:music-property m 'elements)
                      (append (ly:music-property m 'elements)
                         (list (make-script x))))
                   m)
     (else #f)))

	#(define (add-tenuto m)
				(add-script m "tenuto"))

	addTenuto =
			#(define-music-function (parser location music) 
								(ly:music?)
					(map-some-music add-tenuto music))	   
	%%% fin tenuto ............................................

%%%% section \paper  --------------------------------------------------------------
\paper { % #(define page-breaking ly:page-turn-breaking)
    top-margin = 5\mm
    bottom-margin = 1\mm
    head-separation = 5\mm
		foot-separation = 1\mm
		markup-system-spacing = #'((basic-distance . 5) (padding . 3))
		score-markup-spacing = #'((basic-distance  . 5) (padding . 3))
		score-system-spacing = #'((basic-distance  . 10) (padding . 5))
		top-system-spacing = #'((basic-distance . 10) (padding . 5))
		last-bottom-spacing = #'((basic-distance . 13) (padding . 4))
    ragged-last-bottom = ##f
		oddHeaderMarkup = \markup \fill-line { 
				" "
				\on-the-fly #not-first-page \fromproperty #'header:instrumentHeader
				\on-the-fly #print-page-number-check-first \fromproperty #'page:page-number-string }
		evenHeaderMarkup = \markup \fill-line {
				\on-the-fly #print-page-number-check-first \fromproperty #'page:page-number-string
				\on-the-fly #not-first-page \fromproperty #'header:instrumentHeader
				" " }
		oddFooterMarkup = \markup { \fill-line {
				\on-the-fly #first-page \fontsize #-5 
					\left-column { \null \concat { \epsfile #X #9  #"cc2.eps" "http://creativecommons.org"	 }
										"Licence Creative Commons Share-Alike"} 
				\center-column { \general-align #X #CENTER " " \cCopyright " "}
				\on-the-fly #first-page \fontsize #-5 
					\right-column {	\null \concat {"Gravé avec LilyPond " \epsfile #X #3 #"Lily.eps" }
					"http://www.lilypond.org " }					
				}}
}	%% fin paper
