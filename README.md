# Elegie

Reprise d'une transcription de l'Elegie (op.24) de Fauré, par philhar1825 sur [le-concert](http://le-concert.pagesperso-orange.fr/)

Licence: [Creative Commons 3.0 NC-BY-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

Modifications par Romain Maliach-Auguste pour Stravaganza.

## Organisation du repo

* Fichiers .ly : fichiers compilables
* Fichier Global.lyp : fichier de configuration (compositeur, titre, mise en page, fonctions communes, etc...)
* Fichier Trame.lyp : structure de la partition (métrique, tempo, mesures, reprises)
* Fichiers [Instrument].lyp : fichiers de musique (1 fichier par mouvement). Selon l'orchestration,ces fichiers peuvent être regroupés dans un dossier Mus.
* Fichiers .eps : images utilisées pour les pieds de page (logos LilyPond et CreativeCommon) 
